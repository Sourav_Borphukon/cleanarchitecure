package com.shell.sambhu.di

import com.shell.data.di.entityMapperModule
import com.shell.data.di.networkModule
import com.shell.data.di.repositoryModule
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val appModule = module {
    single { }
}

val modules = listOf(
    entityMapperModule,
    appModule,
    networkModule,
    repositoryModule
)