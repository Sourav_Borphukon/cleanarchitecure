package com.shell.sambhu

import android.app.Application
import com.shell.sambhu.di.modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        //start koin!
        startKoin {
            androidContext(this@MainApplication)
            modules(modules)
        }
    }
}