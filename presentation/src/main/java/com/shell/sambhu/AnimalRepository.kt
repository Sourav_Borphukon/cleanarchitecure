package com.shell.sambhu

import androidx.lifecycle.LiveData
import com.shell.domain.model.Animal
import com.shell.domain.repository.base.Repository


interface AnimalRepository : Repository {
    fun getAnimals(): LiveData<Animal>

}