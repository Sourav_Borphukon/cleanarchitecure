object Constant {
    const val DATABASE_NAME="Animal.db"
    const val PREF_API_KEY="Api key"
    const val BASE_URL = "https://us-central1-apis-4674e.cloudfunctions.net"
}