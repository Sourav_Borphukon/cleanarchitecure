package com.shell.data.di

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.shell.data.local.db.AppDatabase
import com.shell.data.local.db.pref.AppPrefs
import com.shell.data.local.db.pref.PrefHelper

import org.koin.dsl.module
import org.koin.experimental.builder.create

val repositoryModule = module {
    single { createDatabaseName() }
    single { createAppDatabase(get(),get()) }
    single { createAnimalDao(get ()) }
    single<PrefHelper> { create<AppPrefs>(get()) }
    single { Gson() }
}

fun createDatabaseName() = Constant.DATABASE_NAME

fun createAppDatabase(dbName: String ,context: Context) =
    Room.databaseBuilder(context, AppDatabase::class.java,dbName).build()

fun createAnimalDao(appDatabase: AppDatabase) = appDatabase.AnimalDao()