package com.shell.data.di


import com.shell.data.model.AnimalEntityMapper
import org.koin.dsl.module
import org.koin.experimental.builder.single

val entityMapperModule = module {
single<AnimalEntityMapper>()
}

