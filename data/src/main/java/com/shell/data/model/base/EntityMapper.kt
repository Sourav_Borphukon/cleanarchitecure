package com.shell.data.model.base

import com.shell.domain.model.base.Model


interface EntityMapper<M: Model, ME: ModelEntity> {
    fun mapToDomain(entity:ME) : M
    fun mapToEntity(model: M): ME


}