package com.shell.data.model

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.shell.data.model.base.ModelEntity
import com.shell.data.model.base.EntityMapper
import com.shell.domain.model.Animal


@Entity(tableName = "animal")
data class AnimalEntity(
    @field: SerializedName("name") val id: String,
    @field: SerializedName("taxonomy") val taxonomy: Taxonomy,
    @field:SerializedName("location") val location: String?,
    @field:SerializedName("speed") val speed: Speed?,
    @field:SerializedName("diet") val diet: String?
) : ModelEntity()


@Entity(tableName = "taxonomy")
data class Taxonomy(
    @field:SerializedName("kingdom") val kingdom: String?,
    @field:SerializedName("order") val order: String?,
    @field:SerializedName("family") val family: String?
)


@Entity(tableName = "speed")
data class Speed(
    @field:SerializedName("metric") val metric: String?,
    @field:SerializedName("imperial") val imperial: String?
)

class AnimalEntityMapper : EntityMapper<Animal, AnimalEntity> {

    override fun mapToEntity(model: Animal) = AnimalEntity (
        id = model.name,
        taxonomy = Taxonomy(kingdom = model.taxonomy?.kingdom,
            order = model.taxonomy?.order,
            family = model.taxonomy?.family
        ),
        location = model.location,
        speed = Speed(
            metric = model.speed?.metrics,
            imperial = model.speed?.imperial

        ),
        diet = model.diet

    )

    override fun mapToDomain(entity: AnimalEntity) = Animal(
        name = entity.id,
        taxonomy = com.shell.domain.model.Taxonomy(
            kingdom = entity.taxonomy?.kingdom,
            order = entity.taxonomy?.order,
            family = entity.taxonomy?.family
        ),
        location = entity.location,
        speed = com.shell.domain.model.Speed(
            metrics = entity.speed?.metric,
            imperial = entity.speed?.imperial

        ),
        diet = entity.diet

    )


}


data class AnimalPalette(var color: Int)
