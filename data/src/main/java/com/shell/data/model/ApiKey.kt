package com.shell.data.model

data class ApiKey (
    val message: String?,
    val key: String
)