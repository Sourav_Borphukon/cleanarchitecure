package com.shell.data.local.db.pref

import android.content.Context


class AppPrefs constructor(mContext: Context,val key: String): PrefHelper
{

    override fun clear() {
        sharedPreferences.edit().clear().apply()
    }

    private var sharedPreferences =
        androidx.preference.PreferenceManager.getDefaultSharedPreferences(mContext.applicationContext)

    fun saveApiKey(key: String?)
    {
        sharedPreferences.edit().putString(Constant.PREF_API_KEY,key).apply()
    }

    fun getApiKey() = sharedPreferences.getString(Constant.PREF_API_KEY,null)

}