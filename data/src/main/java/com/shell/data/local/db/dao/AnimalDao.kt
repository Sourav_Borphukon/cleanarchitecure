package com.shell.data.local.db.dao

import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shell.data.model.AnimalEntity
import io.reactivex.Single

interface AnimalDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(animal: AnimalEntity)

   /* @Query("SELECT * FROM user WHERE id = :id")
    fun findBy(id: String): Single<UserEntity>*/


    @Query("SELECT * FROM animal ")
    fun findBy(id: String): Single<AnimalEntity>

}