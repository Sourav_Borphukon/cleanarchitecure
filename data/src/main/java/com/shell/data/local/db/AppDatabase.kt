package com.shell.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shell.data.local.db.dao.AnimalDao
import com.shell.data.model.AnimalEntity


@Database(entities = [AnimalEntity::class],version = 1,exportSchema = false)
abstract class AppDatabase : RoomDatabase(){

    abstract fun AnimalDao() : AnimalDao
}