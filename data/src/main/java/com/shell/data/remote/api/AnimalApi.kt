package com.shell.data.remote.api

import com.shell.data.model.AnimalEntity
import com.shell.data.model.ApiKey
import io.reactivex.Single
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface AnimalApi {
    @GET("getKey") //Declare end point
    fun getAPIKey() : Deferred<ApiKey> // Single is an observable which returns a single value and finishes

    @FormUrlEncoded
    @POST("getAnimals")
    fun getAnimals(@Field("key") key : String) : Deferred <List<AnimalEntity>>


}