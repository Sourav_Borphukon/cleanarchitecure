package com.shell.domain

import com.shell.domain.model.Animal
import com.shell.domain.repository.base.Repository
import kotlinx.coroutines.channels.ReceiveChannel


interface AnimalRepository : Repository {
   suspend fun getAnimals(): ReceiveChannel<Animal>

}