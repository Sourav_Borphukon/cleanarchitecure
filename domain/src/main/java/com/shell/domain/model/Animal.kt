package com.shell.domain.model

import com.shell.domain.model.base.Model


data class Animal(
    val name: String,
    val taxonomy: Taxonomy?,
    val location: String?,
    val speed:Speed?,
    val diet: String?
) : Model()

data class Taxonomy(
    val kingdom: String?,
    val order: String? ,
    val family: String?
)

data class Speed(
    val metrics: String?,
    val imperial: String?
)